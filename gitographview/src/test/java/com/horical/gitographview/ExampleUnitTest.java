package com.horical.gitographview;

import android.graphics.Color;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void test_Graph() throws Exception {
        List<GraphMultiPoint> lines = new ArrayList<>();
        final Random random = new Random();
        lines.clear();

        List<GraphPoint> points = new ArrayList<>();
        for (int i = 1; i <= 31; i++) {
            GraphPoint graphPoint = new GraphPoint(i, random.nextInt(100));
            points.add(graphPoint);
        }
        lines.add(new GraphMultiPoint("Nesw Task", Color.RED, points));

        points = new ArrayList<>();
        for (int i = 1; i <= 31; i++) {
            GraphPoint graphPoint = new GraphPoint(i, random.nextInt(100));
            points.add(graphPoint);
        }
        lines.add(new GraphMultiPoint("Done Task", Color.BLUE, points));

        points = new ArrayList<>();
        for (int i = 1; i <= 31; i++) {
            GraphPoint graphPoint = new GraphPoint(i, random.nextInt(100));
            points.add(graphPoint);
        }
        lines.add(new GraphMultiPoint("Close Task", Color.GREEN, points));

        points = new ArrayList<>();
        for (int i = 1; i <= 31; i++) {
            GraphPoint graphPoint = new GraphPoint(i, random.nextInt(100));
            points.add(graphPoint);
        }
        lines.add(new GraphMultiPoint("Started Task", Color.YELLOW, points));

        points = new ArrayList<>();
        for (int i = 1; i <= 31; i++) {
            GraphPoint graphPoint = new GraphPoint(i, random.nextInt(100));
            points.add(graphPoint);
        }
        lines.add(new GraphMultiPoint("Ended Task", Color.MAGENTA, points));
    }
}