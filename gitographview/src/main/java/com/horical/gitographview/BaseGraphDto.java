package com.horical.gitographview;

/**
 * Created by nhonvt.dk on 11/1/17
 */

public abstract class BaseGraphDto {
    String name;
    int color;
    private float x;
    private float y;
    double valueOx;
    double valueOy;
    boolean visible;
    boolean showValue;
    private float left;
    private float top;
    private float right;
    private float bottom;

    BaseGraphDto() {
    }

    public BaseGraphDto(double valueOx, double valueOy) {
        this.valueOx = valueOx;
        this.valueOy = valueOy;
    }

    public void setRect(float left, float top, float right, float bottom) {
        this.left = left;
        this.top = top;
        this.right = right;
        this.bottom = bottom;
    }

    public void onToggleVisible() {
        boolean temp = visible;
        this.visible = !temp;
    }

    public void onToggleShowValue() {
        boolean temp = showValue;
        this.showValue = !temp;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public double getValueOx() {
        return valueOx;
    }

    public void setValueOx(double valueOx) {
        this.valueOx = valueOx;
    }

    public double getValueOy() {
        return valueOy;
    }

    public void setValueOy(double valueOy) {
        this.valueOy = valueOy;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isShowValue() {
        return showValue;
    }

    public void setShowValue(boolean showValue) {
        this.showValue = showValue;
    }

    public float getLeft() {
        return left;
    }

    public void setLeft(float left) {
        this.left = left;
    }

    public float getTop() {
        return top;
    }

    public void setTop(float top) {
        this.top = top;
    }

    public float getRight() {
        return right;
    }

    public void setRight(float right) {
        this.right = right;
    }

    public float getBottom() {
        return bottom;
    }

    public void setBottom(float bottom) {
        this.bottom = bottom;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
