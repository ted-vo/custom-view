package com.horical.gitographview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Map;

public class VerticalView extends BaseGraphPartView {

    protected Map<Double, Float> unitOy;
    protected double minValueOy;
    protected double maxValueOy;
    protected int numberOfDistanceOy = 10;
    protected String unitType = "Oy";
    protected int verticalTop;
    protected int verticalBottom;
    protected float distanceOy;
    protected float paddingTop;
    protected float paddingBottom;
    private boolean negative;
    private int width;

    public VerticalView(Context context) {
        super(context);
        init(context);
    }

    public VerticalView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.GraphView,
                0,
                0
        );
        try {
            textSize = typedArray.getDimension(R.styleable.GraphView_textSize, 12);
            textColor = typedArray.getColor(R.styleable.GraphView_textColor, Color.BLACK);
            minValueOy = typedArray.getInteger(R.styleable.GraphView_vertical_minValueOy, 0);
            maxValueOy = typedArray.getInteger(R.styleable.GraphView_vertical_maxValueOy, 10);
            unitType = typedArray.getString(R.styleable.GraphView_vertical_unitOyType);
        } finally {
            typedArray.recycle();
        }
        init(context);
    }

    public VerticalView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @Override
    protected void init(Context context) {
        super.init(context);
        unitOy = new HashMap<>();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        paddingTop = parseDp(10);
        paddingBottom = getBoundsText(unitType).height() + parseDp(15);
        calculatorMeasureWidth();
        setMeasuredDimension(width, heightMeasureSpec);
    }

    public void calculatorMeasureWidth() {
        double maxValue = Math.max(Math.abs(minValueOy), maxValueOy);
        width = (int) parseDp(25) + Math.max(
                getBoundsText(formatDecimal(maxValue)).width(),
                getBoundsText(unitType).width()
        );
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        double maxValue = Math.max(Math.abs(minValueOy), maxValueOy);
        // init unit Oy
        unitOy.clear();
        float height = getHeight() - (int) parseDp(30) - paddingBottom - paddingTop;
        distanceOy = height / numberOfDistanceOy;
        double valuePerDistanceOy;
        float lastValue = getHeight() - paddingBottom;
        if (negative) {
            valuePerDistanceOy = maxValue * 2 / numberOfDistanceOy;
            for (double i = -maxValue; i <= maxValue; i += valuePerDistanceOy) {
                unitOy.put(
                        i,
                        lastValue
                );
                lastValue = lastValue - distanceOy;
            }
        } else {
            valuePerDistanceOy = maxValueOy / numberOfDistanceOy;
            for (double i = 0; i <= maxValueOy; i += valuePerDistanceOy) {
                unitOy.put(
                        i,
                        lastValue
                );
                lastValue = lastValue - distanceOy;
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // draw Oy unit
        paint.setColor(textColor);
        paint.setStyle(Paint.Style.FILL);
        paint.setTextAlign(Paint.Align.RIGHT);
        paint.setPathEffect(null);
        Rect bounds = getBoundsText(unitType);
        canvas.drawText(
                unitType,
                getWidth(),
                verticalTop / 2 + bounds.height() / 4,
                paint
        );

        for (Map.Entry<Double, Float> entry : unitOy.entrySet()) {
            double value = entry.getKey();
            float y = entry.getValue();
            paint.setColor(textColor);
            paint.setStyle(Paint.Style.FILL);
            paint.setTextAlign(Paint.Align.RIGHT);
            paint.setPathEffect(null);
            canvas.drawText(
                    formatDecimal(value),
                    getWidth(),
                    y + bounds.height() / 4, // Center dotted line
                    paint
            );
        }
    }

    private String formatDecimal(double amount) {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
        otherSymbols.setGroupingSeparator(',');
        DecimalFormat formatter = new DecimalFormat("#,###,##0", otherSymbols);
        return formatter.format(amount);
    }

    public Map<Double, Float> getUnitOy() {
        return unitOy;
    }

    public void setUnitOy(Map<Double, Float> unitOy) {
        this.unitOy = unitOy;
    }

    public double getMinValueOy() {
        return minValueOy;
    }

    public void setMinValueOy(double minValueOy) {
        this.minValueOy = minValueOy;
    }

    public double getMaxValueOy() {
        return maxValueOy;
    }

    public void setMaxValueOy(double maxValueOy) {
        if (maxValueOy == 0) maxValueOy = 10;
        this.maxValueOy = maxValueOy;
    }

    public int getMeanSureWidth() {
        return width;
    }

    public int getNumberOfDistanceOy() {
        return numberOfDistanceOy;
    }

    public void setNumberOfDistanceOy(int numberOfDistanceOy) {
        this.numberOfDistanceOy = numberOfDistanceOy;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public void setPaddingTop(int paddingTop) {
        this.paddingTop = paddingTop;
    }

    public void setPaddingBottom(int paddingBottom) {
        this.paddingBottom = paddingBottom;
    }

    public void setVerticalTop(int verticalTop) {
        this.verticalTop = verticalTop;
    }

    public float getDistanceOy() {
        return distanceOy;
    }

    public void setDistanceOy(float distanceOy) {
        this.distanceOy = distanceOy;
    }

    public float getWidthOfUnitType() {
        return getBoundsText(unitType).width();
    }

    public void setVerticalBottom(int verticalBottom) {
        this.verticalBottom = verticalBottom;
    }

    public void setNegative(boolean negative) {
        this.negative = negative;
    }
}
