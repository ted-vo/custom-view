package com.horical.gitographview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.horical.gitographview.GraphView.TYPE_COLUMN;
import static com.horical.gitographview.GraphView.TYPE_COLUMN_IN_COLUMN;
import static com.horical.gitographview.GraphView.TYPE_LINE;

public class HorizontalView extends BaseGraphPartView {
    // Store list point Ox
    protected Map<Integer, Float> unitOx;
    // Store list point Oy
    protected Map<Double, Float> unitOy;
    // String Unit TypeOx
    private String unitType = "Oy";
    protected int maxOx = 31;
    protected float distanceOx;
    private boolean negative;
    private int horizontalTop;
    private int horizontalLeft;
    protected float paddingTop;
    protected float paddingBottom;
    protected float paddingRight;
    private boolean hasFixedSize = false;
    private int typeGraph = TYPE_LINE;
    protected List<GraphMultiPoint> lines;
    private int numberOfDistanceOy = 10;
    protected double minValueOy;
    protected double maxValueOy;
    private int colorHighlight = 0xFF009df7;
    private int highlightValue = -1;
    private boolean deadlineDivider = false;
    private float startDL = -1;
    private float endDL = -1;

    public HorizontalView(Context context) {
        super(context);
        init(context);
    }

    public HorizontalView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.GraphView,
                0,
                0
        );
        try {
            distanceOx = typedArray.getDimension(R.styleable.GraphView_horizontal_distanceOx, 100);
            textSize = typedArray.getDimension(R.styleable.GraphView_textSize, 12);
            textColor = typedArray.getColor(R.styleable.GraphView_textColor, Color.BLACK);
            minValueOy = typedArray.getInteger(R.styleable.GraphView_vertical_minValueOy, 0);
            maxValueOy = typedArray.getInteger(R.styleable.GraphView_vertical_maxValueOy, 0);
        } finally {
            typedArray.recycle();
        }
        init(context);
    }

    public HorizontalView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @SuppressLint("UseSparseArrays")
    @Override
    protected void init(Context context) {
        super.init(context);
        unitOx = new HashMap<>();
        unitOy = new HashMap<>();
        setLayoutParams(
                new LinearLayout.LayoutParams(
                        getContext().getResources().getDisplayMetrics().widthPixels,
                        ViewGroup.LayoutParams.MATCH_PARENT)
        );
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        paddingTop = (int) parseDp(10);
        paddingBottom = getBoundsText(unitType).height() + (int) parseDp(15);
        paddingRight = (int) parseDp(10) + getBoundsText(unitType).width();
        int width;
        int numberOfOx = maxOx;
        if (typeGraph == TYPE_COLUMN || typeGraph == TYPE_COLUMN_IN_COLUMN)
            numberOfOx = maxOx + 1;
        if (hasFixedSize) {
            int screenWidth = getContext().getResources().getDisplayMetrics().widthPixels;
            width = screenWidth - horizontalLeft;
            distanceOx = (width - paddingRight) / numberOfOx;
        } else {
            width = (int) ((numberOfOx * distanceOx) + paddingRight);
        }
        setMeasuredDimension(width, heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        // init unit Oy
        unitOy.clear();
        float height = getHeight() - paddingBottom - paddingTop;
        float distanceOy = height / numberOfDistanceOy;
        double valuePerDistanceOy;
        float lastValue = getHeight() + horizontalTop - paddingBottom;
        if (negative) {
            double maxValue = Math.max(Math.abs(minValueOy), maxValueOy);
            valuePerDistanceOy = maxValue * 2 / numberOfDistanceOy;
            for (double i = -maxValue; i <= maxValue; i += valuePerDistanceOy) {
                unitOy.put(
                        i,
                        lastValue
                );
                lastValue = lastValue - distanceOy;
            }
        } else {
            valuePerDistanceOy = maxValueOy / numberOfDistanceOy;
            for (double i = 0; i <= maxValueOy; i += valuePerDistanceOy) {
                unitOy.put(
                        i,
                        lastValue
                );
                lastValue = lastValue - distanceOy;
            }
        }
        // init unit Ox
        unitOx.clear();
        if (typeGraph == TYPE_LINE) {
            for (int i = 0; i < maxOx; i++) {
                float x = i * distanceOx;
                int key = i + 1;
                if (key <= maxOx)
                    unitOx.put(key, x == 0 ? parseDp(5) : x);
            }
        } else if (typeGraph == TYPE_COLUMN || typeGraph == TYPE_COLUMN_IN_COLUMN) {
            for (int i = 0; i <= maxOx + 1; i++) {
                float x = i * distanceOx;
                unitOx.put(i, x == 0 ? parseDp(5) : x);
            }
        }

        if (typeGraph == TYPE_COLUMN_IN_COLUMN) {
            for (GraphMultiPoint line : lines) {
                double valueOx = line.getValueOx();
                double valueOy = line.getValueOy();
                float xPos = unitOx.get((int) valueOx);
                line.setX(xPos);
                float yPos = calculatorY(valueOy, distanceOy, height);
                line.setY(yPos);
                for (int i = 0; i < line.getPoints().size(); i++) {
                    GraphPoint point = line.getPoints().get(i);
                    point.setX(xPos);
                    float heightWithYPos = getHeight() - paddingBottom - yPos;
                    float pY = (float) point.getValueOy() * heightWithYPos / (float) valueOy;
                    if (i == 0) {
                        point.setY(getHeight() - paddingBottom - pY);
                    } else {
                        float previousY = line.getPoints().get(i - 1).getY();
                        point.setY(previousY - pY);
                    }
                }
            }
        } else {
            for (GraphMultiPoint line : lines) {
                for (GraphPoint point : line.getPoints()) {
                    double valueOx = point.getValueOx();
                    double valueOy = point.getValueOy();
                    point.setX(unitOx.get((int) valueOx));
                    float yPos = calculatorY(valueOy, distanceOy, height);
                    point.setY(yPos);
                }
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        onDrawGraphPad(canvas);
        onDrawUnit(canvas);
        onDrawLines(canvas);
    }

    private void onDrawLines(Canvas canvas) {
        paint.setPathEffect(null);
        switch (typeGraph) {
            case TYPE_LINE: {
                for (GraphMultiPoint line : lines) {
                    if (!line.isVisible()) continue;
                    paint.setStrokeWidth(2);
                    paint.setStyle(Paint.Style.STROKE);
                    paint.setColor(line.getColor());
                    canvas.drawPath(getSmoothPath(line.getPoints()), paint);
                    if (line.isShowValue()) {
                        paint.setStrokeWidth(1);
                        paint.setStyle(Paint.Style.FILL);
                        paint.setTextSize(parseDp(8));

                        for (int i = 0; i < line.getPoints().size(); i++) {
                            GraphPoint graphPoint = line.getPoints().get(i);
                            if (i == 0) {
                                paint.setTextAlign(Paint.Align.LEFT);
                            } else {
                                paint.setTextAlign(Paint.Align.CENTER);
                            }
                            canvas.drawText(
                                    formatDecimal(graphPoint.getValueOy()),
                                    graphPoint.getX(),
                                    graphPoint.getY() - parseDp(5),
                                    paint
                            );
                        }
                    }
                }
                break;
            }
            case TYPE_COLUMN: {
                paint.setStrokeWidth(50);
                paint.setStyle(Paint.Style.FILL);
                float height = getHeight() - paddingBottom - paddingTop;
                float middleZero = height / 2 + paddingTop;
                for (GraphMultiPoint line : lines) {
                    for (int i = 0; i < line.getPoints().size(); i++) {
                        GraphPoint point = line.getPoints().get(i);
                        if (negative) {
                            float desValue = point.getY();
                            if (desValue > middleZero) {
                                desValue = Math.max(desValue, middleZero + parseDp(1));
                                paint.setColor(Color.RED);
                            } else if (desValue < middleZero) {
                                desValue = Math.min(desValue, middleZero - parseDp(1));
                                paint.setColor(line.getColor());
                            } else {
                                double valueOy = point.getValueOy();
                                if (valueOy < 0) {
                                    desValue = middleZero + parseDp(1);
                                    paint.setColor(Color.RED);
                                } else if (valueOy > 0) {
                                    desValue = middleZero - parseDp(1);
                                    paint.setColor(line.getColor());
                                }
                            }
                            canvas.drawLine(
                                    point.getX(),
                                    middleZero,
                                    point.getX(),
                                    desValue,
                                    paint
                            );
                        } else {
                            paint.setColor(line.getColor());
                            canvas.drawLine(
                                    point.getX(),
                                    getHeight() - paddingBottom,
                                    point.getX(),
                                    point.getY(),
                                    paint
                            );
                        }
                    }
                }
                break;
            }
            case TYPE_COLUMN_IN_COLUMN: {
                // draw Column in column
                paint.setStrokeWidth(50);
                paint.setStyle(Paint.Style.FILL);
                for (GraphMultiPoint line : lines) {
                    for (int i = 0; i < line.getPoints().size(); i++) {
                        GraphPoint point = line.getPoints().get(i);
                        paint.setColor(point.getColor());
                        float beginY = i == 0 ? getHeight() - paddingBottom : line.getPoints().get(i - 1).getY();
                        float endY = point.getY();
                        canvas.drawLine(
                                point.getX(),
                                beginY,
                                point.getX(),
                                endY,
                                paint
                        );
                        if (point.getValueOy() != 0) {
                            paint.setColor(0xFFFFFFFF);
                            paint.setTextSize(parseDp(10));
                            String text = formatDecimal(point.getValueOy());
                            canvas.drawText(
                                    text,
                                    point.getX(),
                                    (beginY + endY) / 2 + getBoundsText(text).height() / 3,
                                    paint
                            );
                        }
                    }

                    if (line.getValueOy() != 0) {
                        paint.setColor(0xFF000000);
                        String text = formatDecimal(line.getValueOy());
                        canvas.drawText(
                                text,
                                line.getX(),
                                line.getY() - parseDp(5),
                                paint
                        );
                    }
                }

                // Draw deadline divider
                if (deadlineDivider) { // check visible
                    paint.setStrokeWidth(2);
                    // Draw start
                    paint.setColor(colorHighlight);
                    float xPos = unitOx.get((int) startDL) - distanceOx / 2;
                    canvas.drawLine(
                            xPos,
                            paddingTop,
                            xPos,
                            getHeight(),
                            paint
                    );
                    canvas.drawLine(
                            xPos + parseDp(2),
                            paddingTop,
                            xPos + parseDp(2),
                            getHeight(),
                            paint
                    );
                    canvas.drawText("Start", xPos, paddingTop - getBoundsText("Start").height() / 3, paint);
                    // Draw end
                    paint.setColor(0xFFFF0000);
                    xPos = unitOx.get((int) endDL) + distanceOx / 2;
                    canvas.drawLine(
                            xPos,
                            paddingTop,
                            xPos,
                            getHeight(),
                            paint
                    );
                    canvas.drawLine(
                            xPos + parseDp(2),
                            paddingTop,
                            xPos + parseDp(2),
                            getHeight(),
                            paint
                    );
                    canvas.drawText("End", xPos, paddingTop - getBoundsText("End").height() / 3, paint);
                }
                break;
            }
        }
    }

    /**
     * @param canvas
     */
    private void onDrawUnit(Canvas canvas) {
        // draw Ox unit
        paint.setColor(textColor);
        paint.setTextSize(textSize);
        paint.setStyle(Paint.Style.FILL);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setPathEffect(null);
        Rect bounds = getBoundsText(unitType);
        float bottomText = getHeight() - paddingBottom + bounds.height() + parseDp(5);
        canvas.drawText(
                unitType,
                getWidth() - bounds.width(),
                bottomText,
                paint
        );

        paint.setPathEffect(null);
        for (Map.Entry<Integer, Float> entry : unitOx.entrySet()) {
            int value = entry.getKey();
            float x = entry.getValue();
            if (value <= maxOx) {
                if (value == highlightValue) {
                    paint.setColor(colorHighlight);
                    canvas.drawCircle(x, bottomText - bounds.height() / 3, textSize + parseDp(2), paint);
                    paint.setColor(0xFFFFFFFF);
                } else {
                    paint.setColor(textColor);
                }
                canvas.drawText(
                        String.valueOf(value),
                        x,
                        bottomText,
                        paint
                );
            }
        }
    }

    /**
     * Draw Oxy
     *
     * @param canvas
     */
    protected void onDrawGraphPad(Canvas canvas) {
        // Draw Ox dotted line
        DashPathEffect dashPathEffect = new DashPathEffect(new float[]{5, 5, 5, 5}, 0);
        for (Map.Entry<Double, Float> entry : unitOy.entrySet()) {
            float y = entry.getValue() - horizontalTop;
            paint.setColor(Color.LTGRAY);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(2);
            if (negative && (entry.getKey() == 0)) {
                paint.setPathEffect(null);
            } else {
                paint.setPathEffect(dashPathEffect);
            }
            canvas.drawLine(
                    parseDp(5),
                    y,
                    getWidth() - paddingRight,
                    y,
                    paint
            );
        }

        // Draw Ox
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(2);
        paint.setPathEffect(null);
        canvas.drawLine(
                parseDp(5),
                getHeight() - paddingBottom,
                getWidth() - paddingRight,
                getHeight() - paddingBottom,
                paint
        );

        // Draw Oy dotted line
        for (Map.Entry<Integer, Float> entry : unitOx.entrySet()) {
            float x = entry.getValue();
            paint.setColor(Color.LTGRAY);
            paint.setPathEffect(dashPathEffect);
            paint.setStrokeWidth(2);
            canvas.drawLine(
                    x,
                    parseDp(10),
                    x,
                    getHeight() - paddingBottom,
                    paint
            );
        }
    }

    private Path getSmoothPath(List<GraphPoint> points) {
        Path path = new Path();

        GraphPoint p1 = points.get(0);
        path.moveTo(p1.getX(), p1.getY());

        if (points.size() == 2) {
            GraphPoint p2 = points.get(1);
            path.lineTo(p2.getX(), p2.getY());
            return path;
        }

        for (int i = 1; i < points.size(); i++) {
            GraphPoint p2 = points.get(i);
            GraphPoint middlePoint = midPointForPoints(p1, p2);
            GraphPoint controlPoint1 = controlPointForPoints(middlePoint, p1);
            GraphPoint controlPoint2 = controlPointForPoints(middlePoint, p2);
            path.quadTo(
                    controlPoint1.getX(),
                    controlPoint1.getY(),
                    middlePoint.getX(),
                    middlePoint.getY()
            );
            path.quadTo(
                    controlPoint2.getX(),
                    controlPoint2.getY(),
                    p2.getX(),
                    p2.getY()
            );
            p1 = p2;
        }
        return path;
    }

    /**
     * @param p1
     * @param p2
     */
    private GraphPoint controlPointForPoints(GraphPoint p1, GraphPoint p2) {
        GraphPoint controlPoint = midPointForPoints(p1, p2);
        float diffY = Math.abs(p2.getY() - controlPoint.getY());

        if (p1.getY() < p2.getY())
            controlPoint.setY(controlPoint.getY() + diffY);
        else if (p1.getY() > p2.getY())
            controlPoint.setY(controlPoint.getY() - diffY);

        return controlPoint;
    }

    /**
     * @param p1
     * @param p2
     * @return
     */
    private GraphPoint midPointForPoints(GraphPoint p1, GraphPoint p2) {
        GraphPoint midPoint = new GraphPoint();
        midPoint.setX((p1.getX() + p2.getX()) / 2);
        midPoint.setY((p1.getY() + p2.getY()) / 2);
        return midPoint;
    }

    /**
     * @param valueOy
     * @return
     */
    private float calculatorY(double valueOy, float distanceOy, float height) {
        double valuePerDistanceOy;
        if (negative) {
            double maxValue = Math.max(Math.abs(minValueOy), maxValueOy);
            valuePerDistanceOy = maxValue * 2 / numberOfDistanceOy;
            float middleZero = height / 2 + paddingTop;
            return middleZero - (float) ((valueOy / valuePerDistanceOy) * distanceOy);
        } else {
            valuePerDistanceOy = maxValueOy / numberOfDistanceOy;
            return (float) (getHeight() - (valueOy / valuePerDistanceOy) * distanceOy) - paddingBottom;
        }
    }

    private String formatDecimal(double amount) {
        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
        otherSymbols.setGroupingSeparator(',');
        DecimalFormat formatter = new DecimalFormat("#,###,##0", otherSymbols);
        return formatter.format(amount);
    }

    public void moveToCurrentValue() {
        float y;

    }

    public String getUnitType() {
        return unitType;
    }

    public int getMaxOx() {
        return maxOx;
    }

    public float getDistanceOx() {
        return distanceOx;
    }

    public void setDistanceOx(float distanceOx) {
        this.distanceOx = distanceOx;
    }

    public Map<Integer, Float> getUnitOx() {
        return unitOx;
    }

    public void setUnitOx(Map<Integer, Float> unitOx) {
        this.unitOx = unitOx;
    }

    public Map<Double, Float> getUnitOy() {
        return unitOy;
    }

    public void setUnitOy(Map<Double, Float> unitOy) {
        this.unitOy = unitOy;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public void setMaxOx(int maxOx) {
        this.maxOx = maxOx;
    }

    public boolean isNegative() {
        return negative;
    }

    public void setNegative(boolean negative) {
        this.negative = negative;
    }

    public void setPaddingTop(float paddingTop) {
        this.paddingTop = paddingTop;
    }

    public void setPaddingBottom(float paddingBottom) {
        this.paddingBottom = paddingBottom;
    }

    public void setHorizontalTop(int horizontalTop) {
        this.horizontalTop = horizontalTop;
    }

    public int getHorizontalTop() {
        return horizontalTop;
    }

    public int getHorizontalLeft() {
        return horizontalLeft;
    }

    public void setHorizontalLeft(int horizontalLeft) {
        this.horizontalLeft = horizontalLeft;
    }

    public boolean isHasFixedSize() {
        return hasFixedSize;
    }

    public void setHasFixedSize(boolean hasFixedSize) {
        this.hasFixedSize = hasFixedSize;
    }

    public void setGraphType(int typeGraph) {
        this.typeGraph = typeGraph;
    }

    public void setLines(List<GraphMultiPoint> lines) {
        this.lines = lines;
    }

    public void setNumberOfDistanceOy(int numberOfDistanceOy) {
        this.numberOfDistanceOy = numberOfDistanceOy;
    }

    public void setMinValueOy(double minValueOy) {
        this.minValueOy = minValueOy;
    }

    public void setMaxValueOy(double maxValueOy) {
        this.maxValueOy = maxValueOy;
    }

    public void setHighlightValue(int highlightValue) {
        this.highlightValue = highlightValue;
    }

    public void setColorHighlight(int colorHighlight) {
        this.colorHighlight = colorHighlight;
    }

    public void setDeadline(int start, int end, boolean bool) {
        this.deadlineDivider = bool;
        this.startDL = start;
        this.endDL = end;
    }

    public float getXScroll() {
        int x = 0;
        if (highlightValue == -1) {
            return x;
        }
        return unitOx.get(highlightValue);
    }
}

