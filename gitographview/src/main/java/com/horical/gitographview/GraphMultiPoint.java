package com.horical.gitographview;

import java.io.Serializable;
import java.util.List;

public class GraphMultiPoint extends BaseGraphDto implements Serializable {
    private List<GraphPoint> points;

    public GraphMultiPoint(int color, List<GraphPoint> points) {
        this.points = points;
        this.color = color;
    }

    public GraphMultiPoint(double valueOx, double valueOy, List<GraphPoint> points) {
        this.valueOx = valueOx;
        this.valueOy = valueOy;
        this.points = points;
    }

    public GraphMultiPoint(String name, int color, List<GraphPoint> points) {
        this.name = name;
        this.color = color;
        this.points = points;
        this.visible = true;
        this.showValue = false;
    }

    public List<GraphPoint> getPoints() {
        return points;
    }

    public void setPoints(List<GraphPoint> points) {
        this.points = points;
    }
}
