package com.horical.gitographview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;

import java.util.List;

public class DataView extends BaseGraphPartView {
    protected List<Object> objects;
    protected float paddingLeft;
    protected int dataLeft;
    protected DataViewListener listener;
    protected int graphType = GraphView.TYPE_LINE;
    protected GestureDetectorCompat mDetector;

    public void setListener(DataViewListener listener) {
        this.listener = listener;
    }

    public void setGraphType(int type) {
        this.graphType = type;
    }

    public interface DataViewListener {
        void onDataViewClicked();
    }

    public DataView(Context context) {
        super(context);
        init(context);
    }

    public DataView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.GraphView,
                0,
                0
        );
        try {
            textSize = typedArray.getDimension(R.styleable.GraphView_textSize, 10);
            textColor = typedArray.getColor(R.styleable.GraphView_textColor, Color.BLACK);
        } finally {
            typedArray.recycle();
        }
        init(context);
    }

    public DataView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    protected void init(Context context) {
        super.init(context);
        mDetector = new GestureDetectorCompat(context, new DataViewGestureDetector());
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width;
        if (objects == null) {
            width = getContext().getResources().getDisplayMetrics().widthPixels - dataLeft;
        } else {
            float nextCircleXPos = parseDp(50);
            float nextTextXPos = nextCircleXPos + parseDp(10);
            float y = getHeight() / 2;
            Rect bounds;
            for (Object obj : objects) {
                BaseGraphDto g = (BaseGraphDto) obj;
                bounds = getBoundsText(g.getName());
                g.setRect(
                        nextCircleXPos,
                        y - bounds.height(),
                        nextCircleXPos + bounds.width(),
                        y + bounds.height()
                );

                nextCircleXPos = nextTextXPos + bounds.width() + parseDp(20);
                nextTextXPos = nextCircleXPos + parseDp(10);
            }
            width = (int) nextTextXPos;
        }
        setMeasuredDimension(width, heightMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (objects == null) return;
        float nextCircleXPos = parseDp(50);
        float nextTextXPos = nextCircleXPos + parseDp(10);
        float y = getHeight() / 2;
        Rect bounds;
        for (Object obj : objects) {
            BaseGraphDto g = (BaseGraphDto) obj;
            bounds = getBoundsText(g.getName());
            paint.setStyle(
                    g.isVisible() ?
                            Paint.Style.FILL :
                            Paint.Style.STROKE
            );
            paint.setColor(g.getColor());
            canvas.drawCircle(
                    nextCircleXPos,
                    y - bounds.height() / 4,
                    10,
                    paint
            );

            paint.setColor(textColor);
            paint.setStyle(Paint.Style.FILL);
            paint.setTextAlign(Paint.Align.LEFT);
            canvas.drawText(
                    g.getName(),
                    nextTextXPos,
                    y + bounds.height() / 4,
                    paint
            );

            g.setRect(
                    nextCircleXPos,
                    y - bounds.height(),
                    nextCircleXPos + bounds.width(),
                    y + bounds.height()
            );

            nextCircleXPos = nextTextXPos + bounds.width() + parseDp(20);
            nextTextXPos = nextCircleXPos + parseDp(10);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return this.mDetector.onTouchEvent(event);
    }

    public void setPaddingLeft(float paddingLeft) {
        this.paddingLeft = paddingLeft;
    }

    public void setLines(List<Object> objects) {
        this.objects = objects;
    }

    public void setDataLeft(int dataLeft) {
        this.dataLeft = dataLeft;
    }

    private class DataViewGestureDetector extends GestureDetector.SimpleOnGestureListener {
        private final String DEBUG_TAG = "LOG_DataViewGestureDetector";

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        @Override
        public boolean onDoubleTap(MotionEvent e) {
            float x = e.getX();
            float y = e.getY();

            for (Object obj : objects) {
                BaseGraphDto line = (BaseGraphDto) obj;
                if (x > line.getLeft() &&
                        x < line.getRight() &&
                        y > line.getTop() &&
                        y < line.getBottom()) {
                    line.onToggleShowValue();
                    listener.onDataViewClicked();
                    break;
                }
            }
            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            float x = e.getX();
            float y = e.getY();

            for (Object obj : objects) {
                BaseGraphDto line = (BaseGraphDto) obj;
                if (x > line.getLeft() &&
                        x < line.getRight() &&
                        y > line.getTop() &&
                        y < line.getBottom()) {
                    line.onToggleVisible();
                    invalidate();
                    listener.onDataViewClicked();
                    break;
                }
            }
            return true;
        }
    }
}
